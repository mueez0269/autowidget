package gui.views;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Test extends JInternalFrame {

	private Calculator calc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Test frame = new Test();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Test() {
		setFrameIcon(new ImageIcon(
				Test.class.getResource("/gui/icons/Save-icon.png")));
		setTitle("My test App");
		setBounds(100, 100, 458, 329);
		getContentPane().setLayout(null);

		JButton btnNewButton = new JButton("Enter");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				calc = new Calculator();
				calc.show();

			}
		});
		btnNewButton.setBounds(72, 80, 89, 23);
		getContentPane().add(btnNewButton);

	}
}
