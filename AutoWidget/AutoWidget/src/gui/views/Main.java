package gui.views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;
import java.awt.Color;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.border.LineBorder;

import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JInternalFrame;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.JLabel;

import java.awt.Component;
import java.awt.Font;
import java.awt.ImageCapabilities;
import java.awt.Image;

public class Main extends JFrame {

	private Calculator calc;
	private OperandGetter oprnd;
	private JTable table;
	private JTable table_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setTitle("AutoWidget (designer)");
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				Main.class.getResource("/gui/icons/logo_96.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 817, 624);

		final JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(SystemColor.inactiveCaption);
		desktopPane.setBounds(0, 25, 583, 542);
		getContentPane().add(desktopPane);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnMain = new JMenu("Main");
		menuBar.add(mnMain);

		JMenuItem mntmNewProject = new JMenuItem("New Project");
		mntmNewProject.setIcon(new ImageIcon(Main.class
				.getResource("/gui/icons/Files-New-Window-icon.png")));
		mnMain.add(mntmNewProject);

		JMenuItem mntmOpenExistingProject = new JMenuItem(
				"Open Project");
		mntmOpenExistingProject.setIcon(new ImageIcon(Main.class
				.getResource("/gui/icons/Actions-document-open-icon.png")));
		mnMain.add(mntmOpenExistingProject);

		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.setIcon(new ImageIcon(Main.class
				.getResource("/gui/icons/Save-icon.png")));
		mnMain.add(mntmSave);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		mntmExit.setIcon(new ImageIcon(Main.class
				.getResource("/gui/icons/Actions-application-exit-icon.png")));
		mnMain.add(mntmExit);

		JMenu mnLibrary = new JMenu("Library");
		menuBar.add(mnLibrary);

		JMenu mnWidgets = new JMenu("Widget Library");
		mnWidgets.setIcon(new ImageIcon(Main.class
				.getResource("/gui/icons/opera-widget-icon.png")));
		mnLibrary.add(mnWidgets);

		JMenuItem mntmCalculator = new JMenuItem("Calculator");
		mntmCalculator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				calc = new Calculator();
				desktopPane.add(calc);
				calc.show();

			}
		});
		mnWidgets.add(mntmCalculator);

		JMenuItem mntmOperandGetter = new JMenuItem("Operand Getter");
		mntmOperandGetter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				oprnd = new OperandGetter();
				desktopPane.add(oprnd);
				oprnd.show();

			}
		});
		mnWidgets.add(mntmOperandGetter);

		JMenuItem mntmPointerConnection = new JMenuItem("Pointer Connection");
		mntmPointerConnection.setIcon(new ImageIcon(Main.class
				.getResource("/gui/icons/Ethernet-Cable-icon.png")));
		mnLibrary.add(mntmPointerConnection);
		getContentPane().setLayout(null);

		table = new JTable();
		table.setToolTipText("Properties Window");
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setForeground(SystemColor.activeCaptionText);
		table.setModel(new DefaultTableModel(new Object[][] { { "ID", null },
				{ "JQuery Version", null }, { "File Extension", null },
				{ "Method Type", null }, { "onClick", null },
				{ "DB Source", null }, { "Value Source", null },
				{ "Pointer", null }, }, new String[] { "Name", "Value" }));
		table.getColumnModel().getColumn(0).setPreferredWidth(86);
		table.setBorder(UIManager.getBorder("Button.border"));
		table.setBackground(SystemColor.window);
		table.setBounds(584, 26, 217, 128);
		getContentPane().add(table);

		JLabel lblWidgetContainer = new JLabel("Workspace Pane");
		lblWidgetContainer.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblWidgetContainer.setBounds(0, 0, 130, 26);
		getContentPane().add(lblWidgetContainer);

		JLabel lblWidgetProperties = new JLabel("Widget Properties");
		lblWidgetProperties.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblWidgetProperties.setBounds(584, 0, 130, 26);
		getContentPane().add(lblWidgetProperties);

		JLabel lblPointerProperties = new JLabel("Pointer Properties");
		lblPointerProperties.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPointerProperties.setBounds(584, 158, 171, 26);
		getContentPane().add(lblPointerProperties);

		table_1 = new JTable();
		table_1.setModel(new DefaultTableModel(new Object[][] { { "ID", null },
				{ "Name", null }, { "Source Widget", null },
				{ "Target Widget", null }, { "Connection Type", null }, },
				new String[] { "New column", "New column" }));
		table_1.getColumnModel().getColumn(0).setPreferredWidth(88);
		table_1.setToolTipText("Properties Window");
		table_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_1.setForeground(Color.BLACK);
		table_1.setBorder(UIManager.getBorder("Button.border"));
		table_1.setBackground(Color.WHITE);
		table_1.setBounds(584, 187, 217, 80);
		getContentPane().add(table_1);

	}
}
