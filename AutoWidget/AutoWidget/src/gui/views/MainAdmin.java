package gui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Toolkit;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;

import java.awt.SystemColor;

import javax.swing.JTable;

import java.awt.Color;

import javax.swing.UIManager;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainAdmin extends JFrame {

	private JPanel contentPane;
	private Calculator calc;
	private OperandGetter oprnd;
	private CreateWidget create;
	private JTable table;
	private JTable table_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainAdmin frame = new MainAdmin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainAdmin() {
		setTitle("AutoWidget (developer)");
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainAdmin.class.getResource("/gui/icons/logo_96.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 832, 636);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblWorkspacePane = new JLabel("Workspace Pane");
		lblWorkspacePane.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblWorkspacePane.setBounds(10, 22, 130, 26);
		contentPane.add(lblWorkspacePane);
		
		final JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(SystemColor.inactiveCaption);
		desktopPane.setBounds(0, 48, 591, 552);
		contentPane.add(desktopPane);
		
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 816, 21);
		contentPane.add(menuBar);
		
		JMenu menu = new JMenu("Main");
		menuBar.add(menu);
		
		JMenuItem menuItem = new JMenuItem("New Project");
		menuItem.setIcon(new ImageIcon(MainAdmin.class.getResource("/gui/icons/Files-New-Window-icon.png")));
		menu.add(menuItem);
		
		JMenuItem mntmOpenProject = new JMenuItem("Open Project");
		mntmOpenProject.setIcon(new ImageIcon(MainAdmin.class.getResource("/gui/icons/Actions-document-open-icon.png")));
		menu.add(mntmOpenProject);
		
		JMenuItem menuItem_2 = new JMenuItem("Save");
		menuItem_2.setIcon(new ImageIcon(MainAdmin.class.getResource("/gui/icons/Save-icon.png")));
		menu.add(menuItem_2);
		
		JMenuItem menuItem_3 = new JMenuItem("Exit");
		menuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		menuItem_3.setIcon(new ImageIcon(MainAdmin.class.getResource("/gui/icons/Actions-application-exit-icon.png")));
		menu.add(menuItem_3);
		
		JMenu menu_1 = new JMenu("Library");
		menuBar.add(menu_1);
		
		JMenu mnWidgetLibrary = new JMenu("Widget Library");
		mnWidgetLibrary.setIcon(new ImageIcon(MainAdmin.class.getResource("/gui/icons/opera-widget-icon.png")));
		menu_1.add(mnWidgetLibrary);
		
		JMenuItem menuItem_4 = new JMenuItem("Calculator");
		menuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				calc = new Calculator();
				desktopPane.add(calc);
				calc.show();
			}
		});
		menuItem_4.setIcon(null);
		mnWidgetLibrary.add(menuItem_4);
		
		JMenuItem menuItem_5 = new JMenuItem("Operand Getter");
		menuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				oprnd = new OperandGetter();
				desktopPane.add(oprnd);
				oprnd.show();
				
			}
		});
		menuItem_5.setIcon(null);
		mnWidgetLibrary.add(menuItem_5);
		
		JMenuItem menuItem_6 = new JMenuItem("Pointer Connection");
		menuItem_6.setIcon(new ImageIcon(MainAdmin.class.getResource("/gui/icons/Ethernet-Cable-icon.png")));
		menu_1.add(menuItem_6);
		
		JMenu mnWidget = new JMenu("Widget");
		menuBar.add(mnWidget);
		
		JMenuItem mntmCreateWidget = new JMenuItem("Create Widget");
		mntmCreateWidget.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				create = new CreateWidget();
				create.show();
			}
		});
		mnWidget.add(mntmCreateWidget);
		
		JMenuItem mntmRegisterWidget = new JMenuItem("Register Widget");
		mntmRegisterWidget.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			JFileChooser chooser = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT files","txt");
			chooser.setFileFilter(filter);
			
			Component parent = null;
			int returnVal = chooser.showOpenDialog(parent);
			if(returnVal == JFileChooser.APPROVE_OPTION)
			{ Component frame = null;
			JOptionPane.showMessageDialog(frame, "Widget is Compatible. Added to the Library");
			
			}
			
			}
		});
		mnWidget.add(mntmRegisterWidget);
		
		
		JLabel label_1 = new JLabel("Widget Properties");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_1.setBounds(594, 22, 130, 26);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Pointer Properties");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_2.setBounds(594, 182, 130, 26);
		contentPane.add(label_2);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"ID", null},
				{"jQuery Version", null},
				{"File Extension", null},
				{"Method Type", null},
				{"onClick", null},
				{"DB Source", null},
				{"Source Value", null},
				{"Pointer ID", null},
			},
			new String[] {
				"Name", "Value"
			}
		));
		table.getColumnModel().getColumn(0).setPreferredWidth(85);
		table.setBounds(592, 48, 224, 128);
		contentPane.add(table);
		
		table_1 = new JTable();
		table_1.setModel(new DefaultTableModel(
			new Object[][] {
				{"ID", null},
				{"Name", null},
				{"Source Widget", null},
				{"Target Widget", null},
				{"Connection Type", null},
			},
			new String[] {
				"New column", "New column"
			}
		));
		table_1.getColumnModel().getColumn(0).setPreferredWidth(100);
		table_1.setBounds(594, 209, 222, 80);
		contentPane.add(table_1);
	}
}
