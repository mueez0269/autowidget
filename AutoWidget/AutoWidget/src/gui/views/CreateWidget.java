package gui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JMenuBar;
import javax.swing.JDesktopPane;

import java.awt.Color;

import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

import javax.swing.JTextPane;

import java.awt.SystemColor;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class CreateWidget extends JFrame {

	private JPanel contentPane;
	private XmlEditor editor;
	private OperandGetter operand;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateWidget frame = new CreateWidget();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateWidget() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(CreateWidget.class.getResource("/gui/icons/opera-widget-icon.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 504, 403);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 577, 21);
		contentPane.add(menuBar);
		
		JMenu menu = new JMenu("File");
		menuBar.add(menu);
		
		JMenuItem menuItem = new JMenuItem("Open Project");
		menu.add(menuItem);
		
		JMenuItem menuItem_1 = new JMenuItem("Save Project");
		menu.add(menuItem_1);
		
		JMenuItem menuItem_2 = new JMenuItem("Exit");
		menuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();
			}
		});
		menu.add(menuItem_2);
		
		JMenu mnTools = new JMenu("Tools");
		menuBar.add(mnTools);
		
		final JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(SystemColor.controlShadow);
		desktopPane.setBounds(10, 32, 468, 324);
		contentPane.add(desktopPane);
		
		
		
		JMenuItem mntmSelectIcon = new JMenuItem("Select Icon");
		mntmSelectIcon.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("GIF files","gif");
				chooser.setFileFilter(filter);
				
				Component parent = null;
				int returnVal = chooser.showOpenDialog(parent);
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{ Component frame = null;
				JOptionPane.showMessageDialog(frame, "icon selected");
				}
				
				operand = new OperandGetter();
				desktopPane.add(operand);
				operand.show();
			}
		});
		mnTools.add(mntmSelectIcon);
		
		JMenuItem mntmCreateXml = new JMenuItem("Create XML");
		mntmCreateXml.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editor = new XmlEditor();
				editor.show();
			}
		});
		mnTools.add(mntmCreateXml);
		
		
		
	}
}
